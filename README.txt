----------------------
 Introduction
----------------------
This module scrap facebook cache for proper sharing of content when visitor click share button on an article. As facebook cache the page Organic Graph information as soon as a content is made available. Though the content is being made available via CDN, this content is still not available for public view. Thus the facebook scrap wrong information and when the content is available wrong information is being shared. This is where this module acts in along with ultimate cron. With cron jobs scheduled at regular interval, this module make sure the facebook scrap correct information once the content is available via CDN.

This module cannot be used as-is and is created for custom site purpose only. If you need to same functionality you may have to modify the code to meet as per your need.

-----------------------
 Instruction
-----------------------
1. Install this module as is along with Ultimate Cron or Elysia Cron.

2. Schdule the time even as when to scrap.

As I have explained above there was a time gap of 4 minutes from content
being available on our website. I schedule the fbscrap_cron using ultimate
cron every one minute. While it will run every one minute however the code is
querying data that are published between 4 to 5 minutes from now. So as soon as
the content are made available in CDN every minute, it will send the data to be
scraped again so that right information are being shared.

NOTE: You may have to modify code as per your requirments in fbscrap.module.
Currently it is scrapping only article based content type.

----------------------
Regards
 Sarath Kumar M
 Email: goldenvalley4ever@gmail.com
----------------------